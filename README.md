# My Identity

This repo contains the code behind my personal, electronic visiting card hosted under [david-huber.yomi.eu](https://david-huber.yomi.eu).

## Notes to myself

For development, I used the following hugo watchdog:
```bash
docker run -v $(pwd)/my-identity:/src -v $(pwd)/output:/output -e 'HUGO_THEME=awesome-identity' -e 'HUGO_WATCH=1' -p '1313:1313' jojomi/hugo
```